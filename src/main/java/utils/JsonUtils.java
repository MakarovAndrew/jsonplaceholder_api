package utils;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

@Slf4j
public class JsonUtils {

    private static final ObjectMapper mapper = new ObjectMapper().enable(DeserializationFeature.FAIL_ON_TRAILING_TOKENS);
    private static final ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());

    public static boolean isJsonString(String string) {
        try {
            mapper.readTree(string);
        } catch (JacksonException e) {
            return false;
        }
        return true;
    }

    public static <T> String writeObjectToJsonString(T object) {
        try {
            log.info("Writing Object to the JSON string...");
            return writer.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("Object cannot be converted to a string");
        }
    }

    public static <T> List<T> readObjectsFromJsonString(String jsonString, Class<T> clazz) {
        try {
            log.info("Reading the Object list from the JSON string...");
            return mapper.readValue(jsonString, mapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (IOException e) {
            throw new RuntimeException("Wrong string format!");
        }
    }

    public static <T> T readObjectFromJsonString(String jsonString, Class<T> clazz) {
        try {
            log.info("Reading the Object list from the JSON string...");
            return mapper.readValue(jsonString, clazz);
        } catch (IOException e) {
            throw new RuntimeException("Wrong string format!");
        }
    }

    public static <T> T readObjectFromJsonFile(Path pathToJson, Class<T> clazz) {
        try {
            log.info("Reading Object from file :: \"" + pathToJson + "\" ...");
            return mapper.readValue(pathToJson.toFile(), clazz);
        } catch (IOException e) {
            throw new RuntimeException("Wrong file format or file not found by path: " + pathToJson);
        }
    }
}