package utils;

import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Slf4j
public class HttpUtils {

    private final static int TIMEOUT = 5;

    private final static HttpClient client = HttpClient.newHttpClient();

    public static HttpResponse<String> sendGetRequest(String uri) {
        try {
            log.info("Sending HTTP GET request :: \"" + uri + "\" ...");
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(uri))
                    .GET()
                    .timeout(Duration.ofSeconds(TIMEOUT))
                    .build();
            
            return client.send(request, HttpResponse.BodyHandlers.ofString());

        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpResponse<String> sendPostRequest(String uri, String body) {
        try {
            log.info("Sending HTTP POST request :: \"" + uri+ "\" ...\n" + body);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(uri))
                    .headers("Content-Type", "application/json")
                    .timeout(Duration.ofSeconds(TIMEOUT))
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .build();

            return client.send(request, HttpResponse.BodyHandlers.ofString());

        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}