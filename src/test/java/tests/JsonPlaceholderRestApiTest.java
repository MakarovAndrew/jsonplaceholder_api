package tests;

import api.PostsRequests;
import api.UsersRequests;
import lombok.extern.slf4j.Slf4j;
import models.Post;
import models.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import java.net.http.HttpResponse;
import java.util.List;
import org.testng.asserts.SoftAssert;
import utils.*;
import static utils.ConfigManager.*;

@Slf4j
public class JsonPlaceholderRestApiTest {

    @Test
    public void checkSendingGetAndPostRequests() {

        final SoftAssert softAssert = new SoftAssert();

        log.info("Step #1: Sending GET Request to get all posts...");

            final HttpResponse<String> responseGetAllPosts = PostsRequests.getPosts();
            final List<Post> posts = JsonUtils.readObjectsFromJsonString(responseGetAllPosts.body(), Post.class);

            softAssert.assertEquals(responseGetAllPosts.statusCode(), HttpStatus.SC_OK, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_OK +
                    "\nActual  : " + responseGetAllPosts.statusCode());
            softAssert.assertTrue(JsonUtils.isJsonString(responseGetAllPosts.body()), "The Response body is not a JSON format!" +
                    "\nExpected: true" +
                    "\nActual  : false");
            softAssert.assertTrue(SortingUtils.isSorted(posts, Post::getId), "The Response body is not ascending by id!" +
                    "\nExpected: true" +
                    "\nActual  : false");
            softAssert.assertAll();

        log.info("Step #2: Sending GET request to get post with id=99...");

            final HttpResponse<String> responseGetPostWithId99 = PostsRequests.getPost(testData.getPost99Id());
            final Post post = JsonUtils.readObjectFromJsonString(responseGetPostWithId99.body(), Post.class);

            softAssert.assertEquals(responseGetPostWithId99.statusCode(), HttpStatus.SC_OK, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_OK +
                    "\nActual  : " + responseGetPostWithId99.statusCode());
            softAssert.assertEquals(post.getUserId(), testData.getUserIdForPostWithId99(), "UserId is not equal!" +
                    "\nExpected: " + testData.getUserIdForPostWithId99() +
                    "\nActual  : " + post.getUserId());
            softAssert.assertEquals(post.getId(), testData.getPost99Id(), "Id is not equal!" +
                    "\nExpected: " + testData.getPost99Id() +
                    "\nActual  : " + post.getId());
            softAssert.assertFalse(StringUtils.isBlank(post.getTitle()), "Title is empty!" +
                    "\nExpected: false" +
                    "\nActual  : true");
            softAssert.assertFalse(StringUtils.isBlank(post.getTitle()), "Body is empty!" +
                    "\nExpected: false" +
                    "\nActual  : true");
            softAssert.assertAll();

        log.info("Step #3: Sending GET request to get post with id=150...");

            final HttpResponse<String> responseGetPostWithId150 = PostsRequests.getPost(testData.getPost150Id());

            softAssert.assertEquals(responseGetPostWithId150.statusCode(), HttpStatus.SC_NOT_FOUND, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_NOT_FOUND +
                    "\nActual  : " + responseGetPostWithId150.statusCode());
            softAssert.assertEquals(responseGetPostWithId150.body(), testData.getEmptyResponseBody(), "The Response body is not empty!" +
                    "\nExpected: " + testData.getEmptyResponseBody() +
                    "\nActual  : " + responseGetPostWithId150.body());
            softAssert.assertAll();

        log.info("Step #4: Sending POST request to create post with userId=1 and random body and random title...");

            final int RANDOM_STRING_LENGTH = 40;
            final Post randomPost = Post.builder()
                    .userId(testData.getUserIdForRandomPost())
                    .title(RandomUtils.randomAlphabeticString(RANDOM_STRING_LENGTH))
                    .body(RandomUtils.randomAlphabeticString(RANDOM_STRING_LENGTH))
                    .build();

            final HttpResponse<String> responseCreateRandomPost = PostsRequests.sendPost(randomPost);
            final Post returnedPost = JsonUtils.readObjectFromJsonString(responseCreateRandomPost.body(), Post.class);

            softAssert.assertEquals(responseCreateRandomPost.statusCode(), HttpStatus.SC_CREATED, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_CREATED +
                    "\nActual  : " + responseCreateRandomPost.statusCode());
            softAssert.assertEquals(returnedPost.getUserId(), randomPost.getUserId(), "UserId is not equal!" +
                    "\nExpected: " + randomPost.getUserId() +
                    "\nActual  : " + returnedPost.getUserId());
            softAssert.assertEquals(returnedPost.getTitle(), randomPost.getTitle(), "Title is not equal!" +
                    "\nExpected: " + randomPost.getTitle() +
                    "\nActual  : " + returnedPost.getTitle());
            softAssert.assertEquals(returnedPost.getBody(), randomPost.getBody(), "Body is not equal!" +
                    "\nExpected: " + randomPost.getBody() +
                    "\nActual  : " + returnedPost.getBody());
            softAssert.assertFalse(StringUtils.isBlank(String.valueOf(returnedPost.getId())), "Id is not present!" +
                    "\nExpected: false" +
                    "\nActual  : true");
            softAssert.assertAll();

        log.info("Step #5: Sending GET request to get users...");

            final HttpResponse<String> responseGetAllUsers = UsersRequests.getUsers();
            final List<User> users = JsonUtils.readObjectsFromJsonString(responseGetAllUsers.body(), User.class);
            final User userFromStepFive = UserUtils.getUserByIdFromList(users, testData.getUser5Id());

            softAssert.assertEquals(responseGetAllUsers.statusCode(), HttpStatus.SC_OK, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_OK +
                    "\nActual  : " + responseGetAllUsers.statusCode());
            softAssert.assertTrue(JsonUtils.isJsonString(responseGetAllUsers.body()), "The Response body is not a JSON format!" +
                    "\nExpected: true" +
                    "\nActual  : false");
            softAssert.assertEquals(userFromStepFive, testUser, "Users are not equal!" +
                    "\nExpected: " + testUser.toString() +
                    "\nActual  : " + userFromStepFive.toString());
            softAssert.assertAll();

        log.info("Step #6: Sending GET request to get user with id=5...");

            final HttpResponse<String> responseGetUserWithId5 = UsersRequests.getUser(testData.getUser5Id());
            final User user = JsonUtils.readObjectFromJsonString(responseGetUserWithId5.body(), User.class);

            softAssert.assertEquals(responseGetUserWithId5.statusCode(), HttpStatus.SC_OK, "Wrong status code!" +
                    "\nExpected: " + HttpStatus.SC_OK +
                    "\nActual  : " + responseGetUserWithId5.statusCode());
            softAssert.assertEquals(user, userFromStepFive,"Users are not equal!" +
                    "\nExpected: " + userFromStepFive.toString() +
                    "\nActual  : " + user.toString());
            softAssert.assertAll();
    }
}