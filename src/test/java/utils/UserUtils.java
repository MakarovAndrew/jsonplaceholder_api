package utils;

import models.User;
import java.util.List;

public class UserUtils {

    public static User getUserByIdFromList(List<User> list, int id) {

        return list.stream()
                .filter(searchedUser -> searchedUser.getId() == id)
                .findAny()
                .orElse(null);
    }
}