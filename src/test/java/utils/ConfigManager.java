package utils;

import models.Config;
import models.TestData;
import models.User;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigManager {

    private final static Path PATH_TO_CONFIG = Paths.get("src/test/resources/config.json");
    private final static Path PATH_TO_TEST_DATA = Paths.get("src/test/resources/testData.json");
    private final static Path PATH_TO_TEST_USER = Paths.get("src/test/resources/testUser.json");

    public final static Config config = JsonUtils.readObjectFromJsonFile(PATH_TO_CONFIG, Config.class);
    public final static TestData testData = JsonUtils.readObjectFromJsonFile(PATH_TO_TEST_DATA, TestData.class);
    public final static User testUser = JsonUtils.readObjectFromJsonFile(PATH_TO_TEST_USER, User.class);
}