package models;

import lombok.*;

@NoArgsConstructor @ToString @EqualsAndHashCode @Getter
public class Company {

    private String name;
    private String catchPhrase;
    private String bs;
}