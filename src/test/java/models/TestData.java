package models;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor @Getter
public class TestData {

    private int post99Id;
    private int userIdForPostWithId99;
    private int post150Id;
    private String emptyResponseBody;
    private int userIdForRandomPost;
    private int user5Id;
}