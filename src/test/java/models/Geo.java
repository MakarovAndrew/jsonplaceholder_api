package models;

import lombok.*;

@NoArgsConstructor @ToString @EqualsAndHashCode @Getter
public class Geo {

    private String lat;
    private String lng;
}