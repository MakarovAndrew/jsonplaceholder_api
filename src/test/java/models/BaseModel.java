package models;

import lombok.NoArgsConstructor;
import utils.JsonUtils;

@NoArgsConstructor
public abstract class BaseModel {

    public String toJsonString() {
        return  JsonUtils.writeObjectToJsonString(this);
    }
}