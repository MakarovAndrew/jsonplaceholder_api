package api;

import models.Post;
import utils.ConfigManager;
import utils.HttpUtils;
import java.net.http.HttpResponse;

public class PostsRequests {

    private final static String ENDPOINT = "/posts";
    private final static String URI = ConfigManager.config.getBaseUrl() + ENDPOINT;

    public static HttpResponse<String> getPosts() {
        return HttpUtils.sendGetRequest(URI);
    }

    public static HttpResponse<String> getPost(int postId) {
        return HttpUtils.sendGetRequest(URI + "/" + postId);
    }

    public static HttpResponse<String> sendPost(Post post) {
        return HttpUtils.sendPostRequest(URI, post.toJsonString());
    }
}