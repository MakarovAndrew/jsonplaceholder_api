package api;

import utils.ConfigManager;
import utils.HttpUtils;
import java.net.http.HttpResponse;

public class UsersRequests {

    private final static String ENDPOINT = "/users";
    private final static String URI = ConfigManager.config.getBaseUrl() + ENDPOINT;

    public static HttpResponse<String> getUsers() {
        return HttpUtils.sendGetRequest(URI);
    }

    public static HttpResponse<String> getUser(int userId) {
        return HttpUtils.sendGetRequest(URI + "/" + userId);
    }
}